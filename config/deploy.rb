# config valid for current version and patch releases of Capistrano
lock "~> 3.13.0"

set :application, "shop_bot"
set :repo_url, "git@gitlab.com:tg_shop_bot/shop_bot.git"
set :deploy_to, "/home/Client/shop_bot"

set :keep_releases, 3


append :linked_dirs, 'log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', '.bundle', 'public/system', 'public/uploads', 'storage'
append :linked_files, 'config/database.yml', 'config/secrets.yml', 'config/credentials.yml.enc', 'db/production.sqlite3', 'config/master.key'