class CreatePaymentMethods < ActiveRecord::Migration[5.2]
  def change
    create_table :payment_methods do |t|
      t.string :title
      t.string :number
      t.string :text
      t.boolean :active, default: false
      t.integer :kind, default: 0

      t.timestamps
    end

    add_reference :orders, :payment_method, foreign_key: true
  end
end
