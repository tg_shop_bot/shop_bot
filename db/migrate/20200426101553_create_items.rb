class CreateItems < ActiveRecord::Migration[5.2]
  def change
    create_table :items do |t|
      t.references :category, foreign_key: true
      t.text :text
      t.boolean :active, default: true

      t.timestamps
    end
  end
end
