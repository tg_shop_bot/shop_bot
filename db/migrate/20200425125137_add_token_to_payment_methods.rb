class AddTokenToPaymentMethods < ActiveRecord::Migration[5.2]
  def change
    add_column :payment_methods, :token, :string
  end
end
