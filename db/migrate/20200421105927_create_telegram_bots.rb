class CreateTelegramBots < ActiveRecord::Migration[5.0]
  def change
    create_table :telegram_bots do |t|
      t.string :username
      t.text :text
      t.string :token

      t.timestamps
    end
  end
end
