class CreateCategories < ActiveRecord::Migration[5.0]
  def change
    create_table :categories do |t|
      t.references :telegram_bot, foreign_key: true
      t.string :title
      t.text :text
      t.boolean :active, default: true
      t.integer :position, default: 0
      t.integer :price, default: 0

      t.timestamps
    end
  end
end
