ActiveAdmin.register Category do
  decorate_with CategoryDecorator

  permit_params :telegram_bot_id, :title, :text, :price, :active, :image
  menu priority: 2

  sortable tree: true,
           sorting_attribute: :position,
           parent_method: :parent,
           children_method: :children,
           roots_method: :roots

  index :as => :sortable do
    label :title
    actions
  end

  show do
    attributes_table :telegram_bot, :title, :price, :active do
      row :text do
        category.text.html_safe
      end
      row :image do
        image_tag(url_for(category.image), width: 600) if category.image.attached?
      end
    end
  end

  form do |f|
    f.inputs do
      f.semantic_errors

      f.input :telegram_bot
      f.input :title
      f.input :text, as: :quill_editor, input_html: {data: {options: Category.quill_settings}}
      f.input :price
      f.input :active
      f.input :image, as: :file

      actions
    end
  end
end
