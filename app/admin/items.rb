ActiveAdmin.register Item do
  permit_params :category_id, :text, :active, files: []
  menu priority: 3


  show do
    attributes_table :category, :active do
      row :text do
        item.text.html_safe
      end
      row :files do
        item.files.map do |file|
          link_to file.blob.filename.to_s, url_for(file)
        end
      end
    end
  end

  form do |f|
    f.inputs do
      f.semantic_errors

      f.input :category
      f.input :text, as: :quill_editor, input_html: {data: {options: Item.quill_settings}}
      f.input :active
      f.input :files, as: :file, input_html: { multiple: true }

      actions
    end
  end
end
