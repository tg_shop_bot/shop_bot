ActiveAdmin.register TelegramBot do
  permit_params :username, :text, :image
  menu priority: 1

  show do
    attributes_table :username, :text do
      row :image do
        image_tag(url_for(telegram_bot.image), width: 600) if telegram_bot.image.attached?
      end
    end
  end

  form do |f|
    f.inputs do
      f.semantic_errors

      f.input :username
      f.input :text
      f.input :image, as: :file

      actions
    end
  end
end
