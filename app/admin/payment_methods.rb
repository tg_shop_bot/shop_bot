ActiveAdmin.register PaymentMethod do
  permit_params :kind, :title, :text, :number, :active, :token
  menu priority: 6

  show do
    attributes_table :title, :kind, :number, :token, :active do
      row :text do
        payment_method.text.html_safe
      end
    end
  end

  form do |f|
    f.inputs do
      f.semantic_errors

      f.input :title
      f.input :kind
      f.input :number
      f.input :token
      f.input :text, as: :quill_editor, input_html: {data: {options: PaymentMethod.quill_settings}}
      f.input :active

      actions
    end
  end
end
