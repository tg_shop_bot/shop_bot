# frozen_string_literal: true

class CategoryDecorator < Draper::Decorator
  delegate_all
  include TelegramHelpers

  def to_s
    title
  end

  def decorated_title
    title = object.title
    title = "#{title} (#{price} руб.)" unless price.zero?
    title = "#{title} (неактивен)" unless active
    title
  end

  def buttons
    return pay_buttons unless price.zero?
    children.active.sorted.decorate.map do |item|
      {
          text: item.decorated_title,
          callback_data: {
              type: "category",
              id: item.id
          }.to_json
      }
    end
  end

  def pay_buttons
    PaymentMethod.all.map do |method|
      {
          text: method.title,
          callback_data: {
              type: "new_order",
              category_id: id,
              payment_method_id: method.id
          }.to_json
      }
    end
  end
end