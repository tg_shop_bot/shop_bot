# frozen_string_literal: true

class OrderDecorator < Draper::Decorator
  delegate_all
  include TelegramHelpers

  def text
    str = clean_text(payment_method.text) || ''
    str = str.gsub('{title}', category.title)
    str = str.gsub('{price}', category.price.to_s)
    str = str.gsub('{number}', payment_method.number)
    str = str.gsub('{comment}', id.to_s.rjust(5, '0'))
    str
  end

  def buttons
    [
        {
            text: 'Проверить оплату',
            callback_data:  {
                type: 'check_payment',
                order_id: id
            }.to_json
        }
    ]
  end
end