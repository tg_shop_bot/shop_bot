# frozen_string_literal: true

class TelegramBotDecorator < Draper::Decorator
  delegate_all
  include TelegramHelpers

  def to_s
    title
  end

  private

  def buttons
    Category.roots.active.sorted.map { |x| x.title }
  end

  def keyboard
    {
        keyboard: buttons.each_slice(2).to_a,
        resize_keyboard: false,
        one_time_keyboard: false,
        selective: true
    }
  end
end