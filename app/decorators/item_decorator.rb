# frozen_string_literal: true

class ItemDecorator < Draper::Decorator
  delegate_all
  include TelegramHelpers

  def text
    ActiveStorage::Current.host = Rails.application.credentials.host

    object.text + files.map do |file|
      h.link_to file.blob.filename, file.blob.service_url
    end.join("\n")
  end

  def buttons
    []
  end
end