class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :http_authenticate

  def http_authenticate
    return true unless Rails.env == 'production'
    authenticate_or_request_with_http_basic do |username, password|
      username == Rails.application.credentials.admin[:username] &&
          password == Rails.application.credentials.admin[:password]
    end
  end
end
