class TelegramWebhooksController < Telegram::Bot::UpdatesController
  include Telegram::Bot::UpdatesController::MessageContext
  before_action :set_bot
  before_action :set_user

  def start!(*)
    respond_with *@bot.telegram_message
  end

  def message(data)
    category = find_category(data['text'])

    if category
      respond_with *category.telegram_message
    else
      respond_with :message, text: t('.action_missing.command', command: data['text'])
    end
  end

  def callback_query(data)
    data = JSON.parse(data)

    case data['type']
    when 'category'
      category = find_category(data['id'])
      respond_with *category.telegram_message
    when 'new_order'
      order = create_order(data)
      respond_with *order.telegram_message
    when 'check_payment'
      object = Payment::Qiwi.new(data['order_id']).check&.decorate
      respond_with *object.telegram_message
    else
      respond_with :message, t('.action_missing.command', command: data.to_s)
    end
  end


  def action_missing(action, *_args)
    if action_type == :command
      respond_with :message,
        text: t('telegram_webhooks.action_missing.command', command: action_options[:command])
    else
      respond_with :message, text: t('telegram_webhooks.action_missing.feature', action: action)
    end
  end

  private

  def set_bot
    @bot = TelegramBot.find_by(username: bot.get_me['result']['username'])&.decorate
  end

  def set_user
    @user = User.find_or_create_by(telegram_bot: @bot, chat_id: chat["id"], username: from["username"])
  end

  def find_category(data)
    Finders::Categories.new(@bot, data).perform
  end

  def create_order(data)
    Order.create(user: @user, category_id: data['category_id'], payment_method_id: data['payment_method_id']).decorate
  end
end
