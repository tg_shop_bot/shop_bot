require 'active_support/concern'

module Wysiwygable
  extend ActiveSupport::Concern
  included do
    def self.quill_settings
      {
          modules: {
              toolbar: [['bold', 'italic', 'underline', 'code-block'], ['link'], ['clean']]
          },
          placeholder: 'Введите что-нибудь...', theme: 'snow'
      }
    end
  end
end