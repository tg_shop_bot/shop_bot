require 'active_support/concern'

module Imageable
  extend ActiveSupport::Concern
  included do
    after_save :set_image_filename

    def set_image_filename
      image.blob.update(filename: "#{self.class.to_s.downcase}_#{id}.jpg") if image.attached?
    end

    validates :image, blob: { content_type: ['image/png', 'image/jpg', 'image/jpeg'], size_range: 0..5.megabytes }
  end
end