class PaymentMethod < ApplicationRecord
  include Wysiwygable

  # enums
  enum kind: %w[qiwi btc]
end

# == Schema Information
#
# Table name: payment_methods
#
#  id         :integer          not null, primary key
#  title      :string
#  number     :string
#  text       :string
#  active     :boolean          default(FALSE)
#  kind       :integer          default("qiwi")
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  token      :string
#
