class Item < ApplicationRecord
  include Wysiwygable

  # associations
  has_many_attached :files
  belongs_to :category

  # scopes
  scope :active, -> { where(active: true) }
end

# == Schema Information
#
# Table name: items
#
#  id          :integer          not null, primary key
#  category_id :integer
#  text        :text
#  active      :boolean          default(TRUE)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_items_on_category_id  (category_id)
#
