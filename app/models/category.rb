class Category < ApplicationRecord
  include Wysiwygable
  include Imageable

  # associations
  has_ancestry
  has_one_attached :image
  belongs_to :telegram_bot
  has_many :items, dependent: :destroy
  has_many :orders, dependent: :destroy

  # validations
  validates :title, presence: true

  # scopes
  scope :active, -> { where(active: true) }
  scope :sorted, -> { order(position: :asc) }
end

# == Schema Information
#
# Table name: categories
#
#  id              :integer          not null, primary key
#  telegram_bot_id :integer
#  title           :string
#  text            :text
#  active          :boolean          default(TRUE)
#  position        :integer          default(0)
#  price           :integer          default(0)
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  ancestry        :string
#
# Indexes
#
#  index_categories_on_ancestry         (ancestry)
#  index_categories_on_telegram_bot_id  (telegram_bot_id)
#
