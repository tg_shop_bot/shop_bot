class Order < ApplicationRecord
  # enums
  enum status: %w[await success canceled]

  # associations
  belongs_to :user
  belongs_to :category
  belongs_to :payment_method
end

# == Schema Information
#
# Table name: orders
#
#  id                :integer          not null, primary key
#  user_id           :integer
#  category_id       :integer
#  status            :integer          default("await")
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  payment_method_id :integer
#
# Indexes
#
#  index_orders_on_category_id        (category_id)
#  index_orders_on_payment_method_id  (payment_method_id)
#  index_orders_on_user_id            (user_id)
#
