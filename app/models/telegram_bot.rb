class TelegramBot < ApplicationRecord
  include Imageable
  
  # associations
  has_one_attached :image
  has_many :categories, dependent: :destroy
  has_many :users, dependent: :destroy

  # validations
  validates :username, presence: true
  validates_uniqueness_of :username
end

# == Schema Information
#
# Table name: telegram_bots
#
#  id         :integer          not null, primary key
#  username   :string
#  text       :text
#  token      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
