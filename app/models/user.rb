class User < ApplicationRecord
  # associations
  belongs_to :telegram_bot
  has_many :orders

  # validations
  validates :chat_id, presence: true, uniqueness: true
  validates :username, presence: true
end

# == Schema Information
#
# Table name: users
#
#  id              :integer          not null, primary key
#  username        :string
#  chat_id         :string
#  telegram_bot_id :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
# Indexes
#
#  index_users_on_telegram_bot_id  (telegram_bot_id)
#
