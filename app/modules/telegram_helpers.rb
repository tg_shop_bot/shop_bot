module TelegramHelpers
  def telegram_message
    #return photo_message if defined?(:image) && image.attached? && !Rails.env.development?
    text_message
  end

  def image_url
    ActiveStorage::Current.host = Rails.application.credentials.host
    image.blob.service_url
  end

  def keyboard
    {
        inline_keyboard: buttons.each_slice(2).to_a
    }
  end

  private

  def photo_message
    [
        :photo,
        photo: image_url,
        caption: clean_text(text),
        parse_mode: 'HTML',
        reply_markup: keyboard
    ]
  end


  def text_message
    [
        :message,
        text: clean_text(text),
        parse_mode: 'HTML',
        reply_markup: keyboard
    ]
  end


  def clean_text(text)
    text&.gsub('<p>', '').gsub('</p>', "\n")
  end

  def host
    Rails.application.credentials.host
  end
end