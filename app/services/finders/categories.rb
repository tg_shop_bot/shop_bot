class Finders::Categories
  def initialize(bot, query)
    @bot = bot
    @query = query
  end

  def perform
    category = @bot.categories.where(title: @query).first || @bot.categories.where(id: @query).first
    category&.decorate
  end
end