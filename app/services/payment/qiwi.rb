require 'net/http'

class Payment::Qiwi < Payment::Base
  def payment_success?
    return true
    comment = @order.id.to_s.rjust(5, '0')
    payments_history['data'].select{ |x| x['comment'] == comment}.present?
  end

  def payments_history
    qiwi_number = @order.payment_method.number.tr('+() -', '')
    token = @order.payment_method.token

    uri = URI("https://edge.qiwi.com/payment-history/v2/persons/#{qiwi_number}/payments?rows=20")
    params = { rows: 50, operation: 'IN' }
    uri.query = URI.encode_www_form(params)

    response = Net::HTTP.start(uri.hostname, use_ssl: true) do |http|
      request = Net::HTTP::Get.new uri
      request['Accept'] = 'application/json'
      request['Content-Type'] = 'application/json'
      request['Authorization'] = "Bearer #{token}"
      http.request request
    end
    JSON.parse(response.body)
  end
end