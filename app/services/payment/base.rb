class Payment::Base
  def initialize(order_id)
    @order = Order.find(order_id)
  end

  def check
    return @order unless payment_success? && @order.await?

    complete_order!
    find_item!
    deactivate_category!

    @item
  end

  private

  def complete_order!
    @order.update(status: :success)
  end

  def find_item!
    @item = @order.category.items.active.first
    @item&.update(active: false)
  end

  def deactivate_category!
    @order.category.update(active: false) if @order.category.items.active.blank?
  end

  def payment_success?; end
end